# Light Nuclei Notebooks

Jupyter notebook for INT lattice summer school 2021. Based on these notes: https://arxiv.org/pdf/1608.02563.pdf

Click on the Binder links to run these notebook in the cloud.


| | Day 1 | Day 2 | Day 3 | Day 4 | 
| -| - | - | - | - |
| Notebook | [`transfer_matrix.ipynb`](./transfer_matrix.ipynb) | [`two_point_workbook.ipynb`](./two_point_workbook.ipynb) | [`transfer_matrix_GEVP.ipynb`](./transfer_matrix_GEVP.ipynb)
| Binder link | [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/millernb%2Ftransfer-matrix-notebook/HEAD?filepath=transfer_matrix.ipynb) | [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/millernb%2Ftransfer-matrix-notebook/HEAD?filepath=two_point_workbook.ipynb) | [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/millernb%2Ftransfer-matrix-notebook/HEAD?filepath=transfer_matrix_GEVP.ipynb)
