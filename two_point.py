# two nucleon library
import h5py as h5
import numpy as np
import matplotlib.pyplot as plt

clrs = ['r','g','b']

def read_data(h5file,channel='/pp_SING_OO'):
    data_pp = dict()
    data_p = dict()
#    data_pp['LC'] = np.array(h5file['/pp_SING_OO_LC'])    
    data_pp['PS'] = np.array(h5file[channel + '_PS'])
    data_pp['SS'] = np.array(h5file[channel + '_SS'])
#    data_p['LC'] = np.array(h5file['/prot_LC'])
    data_p['PS'] = np.array(h5file['/prot_PS'])
    data_p['SS'] = np.array(h5file['/prot_SS'])
#    data['pion'] = np.array(h5file[ens + '/pion'])
#    data['kaon'] = np.array(h5file[ens + '/kaon'])
    return np.array([data_p,data_pp])

def effective_mass_ratio(gvdata_p,gvdata_pp,tau=1):
    meff = 1./tau * np.log(gvdata_pp*np.roll(gvdata_p,-tau)**2 / ((gvdata_p*gvdata_p)*np.roll(gvdata_pp,-tau)))
    return meff

def effective_mass(gvdata,tau=1):
    meff = 1./tau * np.log(gvdata / (np.roll(gvdata,-tau)))
    return meff

def plot_meff(gvdata_p,gvdata_pp,lim=[0,20,.95,1.1],tau=1,fname='effective mass'):
    fig = plt.figure(fname,figsize=(7,4.326237/2))
    ax = plt.axes([0.15,0.15,0.8,0.8])
    meff = dict()
    for i,k in enumerate(gvdata_p.keys()):
        print(k)
        x = np.arange(gvdata_p[k].shape[0])
        meff[k] = effective_mass_ratio(gvdata_p[k],gvdata_pp[k],tau=tau)[x]
        ax.errorbar(x=x, y=[d.mean for d in meff[k]], yerr=[d.sdev for d in meff[k]],\
            ls='None', marker='o', color=clrs[i],mec=clrs[i],\
            capsize=2, fillstyle='none')
    ax.axis(lim)
    plt.show()

def plot_meff_fit(gvdata_p,gvdata_pp,params,result,lim=[0,20,.95,1.1],tau=1,fname='eff mass fit'):
    fig = plt.figure(fname)
    ax  = plt.axes([0.15,0.15,0.8,0.8])
    t   = result.data[0]
    dt  = .1
    fit = Fit(params)
    p   = result.p
    x   = np.arange(t[0],t[-1]+dt,dt)
    meff = {}
    for i,k in enumerate(gvdata_p.keys()):
        print(k)
        x = np.arange(gvdata_p[k].shape[0])
        meff[k] = effective_mass_ratio(gvdata_p[k],gvdata_pp[k],tau=tau)[x]
        ax.errorbar(x=x, y=[d.mean for d in meff[k]], yerr=[d.sdev for d in meff[k]],\
            ls='None', marker='o', color=clrs[i],mec=clrs[i],\
            capsize=2, fillstyle='none')

    ax.axis(lim)
    #ax.autoscale(False)   
    for i,tag in enumerate(params['data_select']):
        if len(params['data_select']) > 1:
            tp = fit.two_point_correlator(x,p,tag)
        elif len(params['data_select']) == 1:
            tp = fit.two_point_correlator_A(x,p,tag)
        meff  = 1./dt * np.log(tp / np.roll(tp,-1))
        avg = np.array([d.mean for d in meff])
        err = np.array([d.sdev for d in meff])
        ax.fill_between(x,avg-err,avg+err,color=clrs[i],alpha=.3)
    plt.show()

def plot_zeff(gvdata_p,gvdata_pp,tau=1,xlim=[0,20],ylimS=[0,2e-4],ylimP=[0,.02]):
    zeff = dict()
    for i,k in enumerate(gvdata_pp.keys()):
        x = np.arange(gvdata_pp[k].shape[0])
        meff = effective_mass_ratio(gvdata_p[k],gvdata_pp[k],tau=tau)[x]
        zeff[k] = np.exp(meff * x) * gvdata_pp[k]/(gvdata_p[k]*gvdata_p[k])
    for i,k in enumerate(gvdata_p.keys()):
        fig = plt.figure('effecive z%s' %k,figsize=(7,4.326237/2))
        ax = plt.axes([0.15,0.15,0.8,0.8])
        if k == 'SS':
            ze = np.sqrt(zeff[k])
            lim = [xlim[0],xlim[1],ylimS[0],ylimS[1]]
        elif k == 'PS':
            ze = zeff[k] / np.sqrt(zeff['SS'])
            lim = [xlim[0],xlim[1],ylimP[0],ylimP[1]]
        ax.errorbar(x=x,y=[d.mean for d in ze],yerr=[d.sdev for d in ze],\
            ls='None', marker='o', color=clrs[i],mec=clrs[i],\
            capsize=2, fillstyle='none')
        ax.axis(lim)
        plt.show()

def plot_Aeff(gvdata,tau=1,xlim=[0,20],ylim=[0,.02]):
    zeff = dict()
    for i,k in enumerate(gvdata.keys()):
        fig = plt.figure('effecive A%s' %k,figsize=(7,4.326237/2))
        ax = plt.axes([0.15,0.15,0.8,0.8])
        x = np.arange(gvdata[k].shape[0])
        meff = effective_mass(gvdata[k],tau=tau)[x]
        zeff[k] = np.exp(meff * x) * gvdata[k]
        lim = [xlim[0],xlim[1],ylim[0],ylim[1]]
        ax.errorbar(x=x,y=[d.mean for d in zeff[k]],yerr=[d.sdev for d in zeff[k]],\
            ls='None', marker='o', color=clrs[i],mec=clrs[i],\
            capsize=2, fillstyle='none')
        ax.axis(lim)
        plt.show()

def plot_gs(result):
    fig = plt.figure('effective mass',figsize=(7,4.326237))
    ax = plt.axes([0.15,0.15,0.8,0.8])
    t = result.data[0]
    E0 = result.p['E_0']
    tplot = np.arange(t[0],t[-1]+.1,.1)
    ax.fill_between(tplot,E0.mean-E0.sdev,E0.mean+E0.sdev,color='k',alpha=.2)
    plt.show()


def plot_scor(gvdata):
    fig = plt.figure('scaled correlator',figsize=(7,4.326237))
    ax = plt.axes([0.15,0.15,0.8,0.8])
    x = np.arange(len(gvdata)//2)
    meff = np.log(gvdata/np.roll(gvdata,-1))[x]
    scor = gvdata[x]*np.exp(meff*x)
    ax.errorbar(x = x, y = [d.mean for d in scor], yerr = [d.sdev for d in scor], ls='None', marker='o', capsize=2, fillstyle='none')
    plt.show()

class Fit(object):
    def __init__(self,params):
        self.nstates = params['nstates']
        self.keys = params['data_select']
    def priors(self,prior):
        '''
        only keep priors that are used in fit
        truncate based on n_states
        '''
        p = dict()
        for q in prior:
            if int(q.split('_')[-1].replace(')', '')) < self.nstates:
                p[q] = prior[q]
        return p
    def En(self,p,n):
        '''
        g.s. energy is energy
        e.s. energies are given as log(E_n) = E_n - E_{n-1}
        '''
        E = p['E_0']
        for i in range(1,n+1):
            E += np.exp(p['log(dE_%s)' %(i)])
        return E
    def Zn(self,p,n,tag):
        return p['z%s_%d' %(tag,n)]
    def two_point_correlator(self,x,p,tag):
        r = 0
        for n in range(self.nstates):
            E = self.En(p,n)
            snk,src = tag[0],tag[1]
            z_snk = self.Zn(p,n,snk)
            z_src = self.Zn(p,n,src)
            r += z_snk*z_src*np.exp(-E*x)
        return r
    def two_point_correlator_A(self,x,p,tag):
        r = 0
        for n in range(self.nstates):
            E = self.En(p,n)
            A = self.Zn(p,n,tag)
            r += A*np.exp(-E*x)
        return r
    def fit_function(self,x,p):
        r = dict()
        if len(self.keys)>1:
            for k in self.keys:
                r[k] = self.two_point_correlator(x,p,k)
        elif len(self.keys) == 1:
            r[self.keys[0]] = self.two_point_correlator_A(x,p,self.keys[0])
        return r

class FitMeson(object):
    def __init__(self,params):
        self.nstates = params['nstates']
        self.keys = params['data_select']
        self.tmax = params['tmax']
    def priors(self,prior):
        '''                                                                                                  
        only keep priors that are used in fit                                                                
        truncate based on n_states                                                                           
        '''
        p = dict()
        for q in prior:
            if int(q.split('_')[-1]) < self.nstates:
                p[q] = prior[q]
        return p
    def En(self,p,n):
        '''                                                                                                  
        g.s. energy is energy                                                                                
        e.s. energies are given as log(E_n) = E_n - E_{n-1}                                                  
        '''
        E = p['E_%d' %n]
        for i in range(1,n+1):
            E += np.exp(p['E_%s' %(i)])
        return E
    def Zn(self,p,n,tag):
        return p['z%s_%d' %(tag,n)]
    def two_point_correlator(self,x,p,tag):
        r = 0
        for n in range(self.nstates):
            E = self.En(p,n)
            snk,src = tag[0],tag[1]
            z_snk = self.Zn(p,n,snk)
            z_src = self.Zn(p,n,src)
            r += z_snk*z_src*(np.exp(-E*x)+np.exp(-E*(np.roll(x,-1)-x))) 
        return r
    def two_point_correlator_A(self,x,p,tag):
        r = 0
        for n in range(self.nstates):
            E = self.En(p,n)
            A = self.Zn(p,n,tag)
            r += A*np.exp(-E*x)
        return r
    def fit_function(self,x,p):
        r = dict()
        if len(self.keys)>1:
            for k in self.keys:
                r[k] = self.two_point_correlator(x,p,k)
        elif len(self.keys) == 1:
            r[self.keys[0]] = self.two_point_correlator_A(x,p,self.keys[0])
        return r
